# design-pattern

Step 1:
Run 'npm i'

Step 2:
Run 'tsc' in terminal


Design patterns implemented

Creational - 
1. Factory
2. Singleton
3. Abstract Factory
4. Builder
5. Prototype


Structural
1. Adapter
2. State
3. Facade
4. Bridge
5. Composite
6. Proxy
7. Flyweight

Behavioural
1. Decorator
2. Observe
3. Chain of responsibility
4. Command
5. Iterator
6. Strategy
7. Mediator
8. Memento
10. Template
11. Visitor