import { Resp } from '../Resp'; // use path npm
export interface Model {
    createTable(): Resp; // Need to return an response class which has a proper format instead of JSON while writing the code.
    updateTable(tablename: String, id: String): Resp; // Need to return an response class which has a proper format instead of JSON while writing the code.
}