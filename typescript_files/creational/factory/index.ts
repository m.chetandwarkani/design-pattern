import { OrmCreator } from './DAOImpl/ormFactory'; // use path npm
import { Model } from './DAO/orm'; // use path npm
export class models {
    ormtenant: Model = new OrmCreator().create('PRISMA');
    ormtenant2: Model = new OrmCreator().create('SEQUILIZE');

    constructor() {
        console.log(" ---------------------------- Factory design pattern -----------------------")

        let resp = this.ormtenant.createTable();
        let resp2 = this.ormtenant2.createTable();
        
        console.log(" ---------------------------- End of Factory design pattern -----------------------")
    }
}