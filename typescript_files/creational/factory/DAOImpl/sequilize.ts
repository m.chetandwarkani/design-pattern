import { Model } from '../DAO/orm'; // use path npm
import { Resp } from '../Resp'; // use path npm
export class Sequilize implements Model {
    name: String;
    constructor(){
        this.name='SEQUILIZE'
    }
    updateTable(tablename: String, id: String): Resp {
        return new Resp(200, 'updated table using sequilize');
    }
    public createTable(): Resp {
        //create table using Prisma
        return new Resp(200, 'created table using sequilize');
    }

 
}