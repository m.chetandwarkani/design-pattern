import { Model } from '../DAO/orm'; // use path npm
import { Resp } from '../Resp'; // use path npm
export class Prisma implements Model{
    name: String;
    constructor(){
        this.name='PRISMA'
    }
    updateTable(tablename: String, id: String): Resp {
        return new Resp(200,'updated table using Prisma');
    }
    public createTable(): Resp {
        //create table using Prisma
        return new Resp(200,'created table using Prisma');
    }
}