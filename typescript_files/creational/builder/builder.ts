export interface Isubsriptions{
    prevstate: string
    newstate: string
    plan: string
    id: number
}

class subscriptionActivity implements Isubsriptions{
    prevstate!: string
    newstate!: string
    plan!: string
    id!: number
    getPrevState(){

    }
    setPrevState(s: Isubsriptions['prevstate']){
        this.prevstate =s;
        return this;
    }
    setNewState(s: Isubsriptions['newstate']){
        this.newstate =s;
        return this;
    }

}
let s = new subscriptionActivity();
s.setPrevState('FREE').setNewState('ACTIVE');