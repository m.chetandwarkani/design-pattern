export class Resp{
    private code: number;
    private message: String;
    constructor(code: number, message: String){
        this.code = this.validateCode(code);
        this.message = message;
    }
    
    validateCode(code: number): number{
        if([200,300,400,401,403,500].includes(code)){
            return code;
        }else{
            throw new Error("Code not found.")
        }
    }
}