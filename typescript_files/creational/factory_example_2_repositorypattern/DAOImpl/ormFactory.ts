import { Model } from '../DAO/orm'; // use path npm
import { Prisma } from './prisma'; // use path npm
import { Sequilize } from './sequilize'; // use path npm
export class OrmCreator {
    public create(objecttype: String): Model {
        switch (objecttype) {
            case 'PRISMA':
                return new Prisma();
            case 'SEQUILIZE':
                return new Sequilize();
            default:
                return new Sequilize();
        }
    }
}