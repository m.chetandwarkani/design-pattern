import { models } from '../../DAO/models';
import { DTO } from '../../DTO/DTO';
import { OrmCreator } from '../ormFactory';
import { Model } from '/Users/chetandwarkani/Desktop/Chetan/design-pattern/typescript_files/creational/factory_example_2_repositorypattern/DAO/orm';
export class Subscriptipon implements models {
    ormtenant: Model;
    constructor() {
        this.ormtenant = new OrmCreator().create('SEQUILIZE');
    }
    
    createTable(): void {
        throw new Error('Method not implemented.');
    }

    updateTable(tenantdto: DTO, options: object): void {
        console.log("updateTable")
        this.ormtenant.updateTable('Tenant',tenantdto, options)
        //this.ormtenant.updateTable using prisma ORM
    }
    
    findbyID(id: number): void {
        console.log("findbyID " + id)
        //this.ormtenant.findbyid using prisma ORM
    }
}