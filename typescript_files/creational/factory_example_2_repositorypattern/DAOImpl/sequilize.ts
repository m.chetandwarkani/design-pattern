import { Model } from '../DAO/orm'; // use path npm
import { DTO } from '../DTO/DTO';
import { Resp } from '../Resp'; // use path npm
export class Sequilize implements Model {
    name: String;
    constructor(){
        this.name='SEQUILIZE'
    }
    updateTable(tablename: String, dto: DTO, params: object): Resp {
        //const currentShard: SequelizeModels = await this.database.getShardModels({ modelName: table });
        //const newCompany = await currentShard.Table.update({ ...dto }, { ...params });
        return new Resp(200, 'updated table using sequilize');
    }
    public createTable(): Resp {
        
        //create table using Prisma
        return new Resp(200, 'created table using sequilize');
    }

 
}