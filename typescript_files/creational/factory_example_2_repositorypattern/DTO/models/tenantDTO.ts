import {DTO} from '../DTO'
export class TenantDTO implements DTO{
    id!: bigint;
    tenantid!: string;
    tenantname!: string;
    tenantkeyhas!: string;
    public getID() {
        return this.id;
    }
    public setID(id: bigint) {
        this.id = id;
    }
    public getTenantName() {
        return this.tenantname;
    }
    public setTenantName(name: string) {
        this.tenantname = name;
    }
}