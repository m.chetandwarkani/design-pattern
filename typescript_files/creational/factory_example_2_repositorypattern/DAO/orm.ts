import { DTO } from '../DTO/DTO';
import { Resp } from '../Resp'; // use path npm
export interface Model {
     // Need to return an response class which has a proper format instead of JSON while writing the code.
    updateTable(tablename: String, dto: DTO, options: object): Resp; // Need to return an response class which has a proper format instead of JSON while writing the code.
}