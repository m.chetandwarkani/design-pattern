import { DTO } from "../DTO/DTO";

export interface models{
    createTable(): void;
    updateTable(tenantdto: DTO, options: object) : void;
    findbyID(id: number): void;
}