This design pattern is useful when we wish to have the flexibilty of creating objects dynamically based on a certain input. 
Also, when we wish to have flexibilty of adding super easy plug and play new versions of the service classes. 

One classic example where this service is useful when we wish to utilize different ORMs where one ORM is better in find speed and other in write speed. 

Another classic example is when we have scope of supporting multiple paymemt gateways in our app. 