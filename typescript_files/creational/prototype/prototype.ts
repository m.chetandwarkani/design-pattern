interface IProtoype {
    deepCopy(tenantObj: IProtoype): IProtoype
}

class Tenant implements IProtoype {
    constructor() {
    }

    deepCopy(tenantObj: IProtoype): IProtoype {
        return JSON.parse(JSON.stringify(tenantObj));
    }
}