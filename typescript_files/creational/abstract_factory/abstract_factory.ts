interface paymentservice {
    model: Model;
    payments: Payments;
}


//Models Feature

export interface Model {
    createTable(): Resp; // Need to return an response class which has a proper format instead of JSON while writing the code.
    updateTable(tablename: String, id: String): Resp; // Need to return an response class which has a proper format instead of JSON while writing the code.
}

export class Prisma implements Model {
    name: String;
    constructor() {
        this.name = 'PRISMA'
    }
    updateTable(tablename: String, id: String): Resp {
        return new Resp(200, 'updated table using Prisma');
    }
    public createTable(): Resp {
        //create table using Prisma
        return new Resp(200, 'created table using Prisma');
    }
}

export class Sequilize implements Model {
    name: String;
    constructor() {
        this.name = 'SEQUILIZE'
    }
    updateTable(tablename: String, id: String): Resp {
        return new Resp(200, 'updated table using sequilize');
    }
    public createTable(): Resp {
        //create table using Prisma
        return new Resp(200, 'created table using sequilize');
    }


}

export class Resp {
    private code: number;
    private message: String;
    constructor(code: number, message: String) {
        this.code = this.validateCode(code);
        this.message = message;
    }

    validateCode(code: number): number {
        if ([200, 300, 400, 401, 403, 500].includes(code)) {
            return code;
        } else {
            throw new Error("Code not found.")
        }
    }
}



//Payment Service Provider Feature

export interface Payments {
    createSubscription(tableName: String): void; // Need to return an response class which has a proper format instead of JSON while writing the code.
    updateSubscription(tablename: String, id: String): void; // Need to return an response class which has a proper format instead of JSON while writing the code.
}

export class Chargebee implements Payments {
    name: String;
    constructor() {
        this.name = 'PRISMA'
    }
    createSubscription(tableName: String) {
        console.log("create sub using chargbee")
    }
    updateSubscription(tablename: String, id: String) {
        console.log("update sub using chargbee")
    }


}

export class Stripe implements Payments {
    name: String;
    constructor() {
        this.name = 'SEQUILIZE'
    }
    createSubscription(tableName: String) {
        console.log("create sub using Stripe")
    }
    updateSubscription(tablename: String, id: String) {
        console.log("update sub using Stripe")
    }

}

