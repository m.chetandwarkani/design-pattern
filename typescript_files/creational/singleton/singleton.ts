export class Repo{
    private constructor(){
        // private is to prevent instanitaing and creating an object out of it
    }
    public static repos(num: any){
        switch (num) {
            case num==1:
                return 'ONE'
            case num==2:
                return 'TWO'
            default:
                return 'ONE';
        }
    }
}


// let repo = new Repo();  - This is prevented by private constructor
Repo.repos(1);
Repo.repos(2);