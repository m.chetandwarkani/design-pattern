// Like a cache - rather than creating multiple objects of very same matching type, we can have one object holding details on the same.
class FreePlan {
    static plan: string = 'FREE'
    static planFetaure: string = 'migrate, submission';
    public static getplandetail() {
        return this.plan + "   " + this.planFetaure;
    }

}

class BusinessPlan {
    static plan: string = 'Busines'
    static planFetaure: string = 'migrate, submission';
    public static getplandetail() {
        return this.plan + "   " + this.planFetaure;
    }
}

// here rather than creating objects of free plan separately inside subscription class, we can simply set the type in subscription class and get relavant details reusing the same object

class subscription {
    type: string = 'FREE';
    public setType(type: string) {
        this.type = type;
    }
    public getFeatures() {
        switch (this.type) {
            case 'FREE':
                return FreePlan.getplandetail();
            case 'BUSINESS':
                return BusinessPlan.getplandetail();
        }

    }
}