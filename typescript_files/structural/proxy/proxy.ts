class SubService {
    constructor() {

    }
    request(){
        console.log("subservice")
    }
}

class Helper {
    subService: SubService;
    constructor(subService: SubService) {
        this.subService = subService;
    }

    request(){
        console.log("act on service body request for validation check")
        this.subService.request();
    }
}

let subServ = new SubService();
subServ.request();
subServ = new Helper(subServ);
subServ.request();