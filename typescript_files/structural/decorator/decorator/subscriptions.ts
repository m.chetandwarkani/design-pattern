import {Subscription} from '../mainclass/subs'
export abstract class SubscriptionOptions extends Subscription{
    public abstract decoratedSubs: Subscription;
    public abstract description: String;
}

export class updateMasterPlanInDB extends SubscriptionOptions{
    public decoratedSubs: Subscription;
    public description: String = 'Master Plan updated';
    constructor(decoratedSubs: Subscription){
        super();
        this.decoratedSubs = decoratedSubs;
        //update master plan name and details
    }
    public price(): number {
        
        // fetch price for the relevant master plan
        // let fetched master plan price be = 3000
        return this.decoratedSubs.price() + 3000;
    }
}

export class updateAddonInDB extends SubscriptionOptions{
    public decoratedSubs: Subscription;
    public description: String = 'Master Plan updated';
    constructor(decoratedSubs: Subscription){
        super();
        this.decoratedSubs = decoratedSubs;
        //update addon plan name and details
    }
    public price(): number {
        // fetch price for the relevant addon plan
        // let fetched master plan price be = 3000
        return this.decoratedSubs.price() + 1000;
    }
}