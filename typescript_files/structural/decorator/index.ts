import {updateMasterPlanInDB, updateAddonInDB} from './decorator/subscriptions'
import {Stripe, Chargebee} from './mainclass/subs'
let subs = new Stripe();
subs = new updateMasterPlanInDB(subs);
subs = new updateAddonInDB(subs);
subs.updateSubscription();

let subs2 = new Chargebee();
subs2 = new updateMasterPlanInDB(subs2);
subs2 = new updateAddonInDB(subs2);
subs2.updateSubscription();



