export abstract class Subscription{
    public abstract  description: String;
    public getdescription(){
        return this.description;
    }
    public abstract price(): number;
    public updateStripeSubscription(){
        
    }
    public updateSubscription(){
        
    }
}

export class Stripe extends Subscription{
    public description: String = 'Stripe Subscription';
    
    public price(): number {
        return 0;
    }
    public updateSubscription(){
        // update Stripe Subscription
    }
}

export class Chargebee extends Subscription{
    public description: String = 'Chargebee Subscription';
    public price(): number {
        return 0;
    }
    public updateSubscription(){
        //// update chargebee Subscription
    }
}





