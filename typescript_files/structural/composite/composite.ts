interface Isubsriptions{
    updatePlan(plan: String): void;
}
class Subscription implements Isubsriptions {
    plan: String ='FREE';
    constructor(){

    }
    updatePlan(plan: String): void {
        this.plan=plan;
    }
    
}

//composite class basically acts on set of objects
class compositeSubscriptions implements Isubsriptions{
    subs: Subscription[]=[];
    constructor(){
    }
    addSubscription(sub: Subscription){
        this.subs.push(sub)
    }
    updatePlan(plan: String): void {
        for(let i=0;i<this.subs.length;i++){
            this.subs[i].plan=plan;
        }
    }
    
}