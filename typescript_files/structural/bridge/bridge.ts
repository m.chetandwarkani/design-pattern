abstract class Shape{
    draw(){
        console.log("Drawing")
    }
    abstract getRadius(): void
}

class Circle extends Shape{
    getRadius(): void {
        console.log("radius ")
    }
}

class Rectangle extends Shape{
    getRadius(): void {
        console.log("")
    }
}

abstract class Color {
    abstract shape: Shape
    abstract color:  String;
    abstract updateColor(): void;
}

class Red extends Color{
    color: String ='';
    shape: Shape;
    constructor(shape: Shape ){
        super();
        this.shape = shape;
    }
    updateColor(): void {
        this.color= 'red';
        console.log("color changes to red")
    }

}

class Blue extends Color{
    color: String ='';
    shape: Shape;
    
    constructor(shape: Shape ){
        super();
        this.shape = shape;
    }

    updateColor(): void {
        this.color= 'BLUE';
        console.log("color changes to BLUE")
    }
}