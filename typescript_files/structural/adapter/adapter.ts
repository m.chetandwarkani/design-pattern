interface Charger{
    connectToCharge(): void;
}
class Iphone implements Charger{
    connectToCharge(): void {
        console.log('connected to type C')
    }
}
class Android implements Charger{
    connectToCharge(): void {
        console.log('connected to Lighting cable')
    }
}


class AndroidToIphoneCharger implements Charger{
    public constructor( android :Charger) {
        throw new Error("Method not implemented.");
    }
    connectToCharge(): void {
        // Make type c to lihtig available by accessing android properties.
        console.log("Type C to Lightning connected")
    }
}

let iphone = new Iphone();
let android = new Android();
iphone = new AndroidToIphoneCharger(iphone);