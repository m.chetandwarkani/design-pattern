abstract class Subscriptions{
    abstract create();
    
    abstract update();

    sendSubEmail(sub){
        console.log("subscription created succesfully.")
    } 
}

class Stripe extends Subscriptions{
    create() {
        console.log("create sub using stripe")
    }
    update() {
        console.log("update sub using stripe")
    }
    
}

class ChargeBee extends Subscriptions{
    create() {
        console.log("create sub using ChargeBee")
    }
    update() {
        console.log("update sub using ChargeBee")
    }
    
}