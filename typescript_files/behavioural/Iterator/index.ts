interface ISubscription{
    setPlan(): void;
    setSubID(): void;
}

class Subscription implements ISubscription{
    setSubID(): void {
        console.log("")
    }
    setPlan(): void {
        console.log("")
    }
}

interface SubIterator{
    hasMore(): void;
    getNext(): void;
}
class SubcriptionIterator implements SubIterator{
    subscriptions: Subscription[];
    pos: number = 0;
    addSubscription(subscription: Subscription){
        this.subscriptions.push(subscription);
    }
    hasMore(): void {
        //based on pos check if more subs are there
    }
    getNext(): void {
        this.pos++;
         //based on pos check if more subs are there
    }
}
let sub1 = new Subscription();
let sub2 = new Subscription();

let sI = new SubcriptionIterator();
sI.addSubscription(sub1)
sI.addSubscription(sub2)