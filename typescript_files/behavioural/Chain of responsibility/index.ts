abstract class Handler {
    handler: Handler;
    setHandler(handler: Handler): void {
        this.handler = handler;
    }
    abstract handlerequest(): void;
}

class Router extends Handler{
    handler: Handler;
    setHandler(handler: Handler): void {
        this.handler = handler;
    }
    handlerequest(): void {
        throw new Error("Method not implemented.");
    }

}


class Auth extends Handler{
    handlerequest(): void {
        console.log("check user auth");
        this.handler.handlerequest();
    }

}

class Mainfunc extends Handler{
    handler: Handler;
    setHandler(handler: Handler): void {
        this.handler = handler;
    }
    handlerequest(): void {
        console.log("execute key processing logics");
        
    }

}

let router = new Router();
let auth = new Auth();
let mainfunc = new Mainfunc();
auth.setHandler(mainfunc)
router.setHandler(auth);
router.handlerequest();


