interface ObserveSubject {
    update(): String;
}

interface Observer {
    addObserver(observe: ObserveSubject): String;
    removeObserver(observe: ObserveSubject): String;
    notify(observe: ObserveSubject): String;
}

class Stripewebhooks implements Observer {
    private observers: ObserveSubject[] = [];
    public addObserver(observerSubject: ObserveSubject): String {
        this.observers.push(observerSubject)
        return 'Observer added'
    }
    public removeObserver(observe: ObserveSubject): String {
        this.observers.pop()
        return 'Observer removed'
    }
    public notify(observe: ObserveSubject): String {
        for (let i = 0; i < this.observers.length; i++) {
            this.observers[i].update();
        }
        return 'Observer notified'
    }
}

class updateDB implements ObserveSubject {
    public update(): String {
        console.log('DB updated')
        return 'DB updated'
    }

}

class updateHubspot implements ObserveSubject {

    public update(): String {
        console.log('Hubspot updated')
        return 'DB updated'
    }

}

class mainclass {
    constructor() {
        let stripewebhooks = new Stripewebhooks();
        let updatedb: ObserveSubject = new updateDB()
        let updatehubspot = new updateHubspot()
        stripewebhooks.addObserver(updatedb)
        stripewebhooks.addObserver(updatehubspot)
    }
}
