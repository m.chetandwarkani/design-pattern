// Let's say we have a button which is can be made useful to switch between multiple performance based on requirement

interface Command {
    execute(): void;
}

class TurnOnFan implements Command {
    fan: Fan
    constructor(fan: Fan) {
        this.fan = fan;
    }
    execute(): void {
        this.fan.turnon();
    }

}

class TurnOffFan implements Command {
    fan: Fan;
    constructor(fan: Fan) {
        this.fan = fan;
    }
    execute(): void {
        this.fan.turnoff();
    }

}


class Fan {
    turnoff() {
        console.log("Turning off fan")
    }
    turnon() {
        console.log("Turning on fan")
    }
}


class TurnOnLight implements Command {
    light: Light
    constructor(light: Light) {
        this.light = light;
    }
    execute(): void {
        this.light.turnon();
    }

}

class TurnOffLight implements Command {
    light: Light;
    constructor(light: Light) {
        this.light = light;
    }
    execute(): void {
        this.light.turnoff();
    }

}


class Light {
    turnoff() {
        console.log("Turning off Light")
    }
    turnon() {
        console.log("Turning on Light")
    }
}


class Button {
    command: Command;
    setCommand(command: Command) {
        this.command = this.command;
    }
    pressButton() {
        this.command.execute();
    }
}
let light = new Light();
let fan = new Fan();
let button = new Button();
button.setCommand(new TurnOffFan(fan));
button.pressButton();
button.setCommand(new TurnOnFan(fan));
button.pressButton();
button.setCommand(new TurnOnLight(light));
button.pressButton();