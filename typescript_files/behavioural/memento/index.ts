class Add {
    a: number;
    b: number;
    setA(a: number){
        this.a = a;
    }
    setB(b: number){
        this.b = b;
    }
}

class Snapshot {
    a: number;
    b: number;
    add: Add;
    constructor(add: Add, a: number, b: number) {
        this.add = add;
        this.a = a;
        this.b = b;
    }

    restore(){
        this.add.setA(this.a);
        this.add.setB(this.a);
    }

}