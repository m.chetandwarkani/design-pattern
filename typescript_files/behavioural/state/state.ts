interface State{
    cancelOrder(): void;
    verifyOrderPayment(): void;
    shipProgress(): void;
}

class Order implements State{
    state: String;
    constructor(){
        this.state='Payment Pending';
    }
    cancelOrder(){
        console.log("order cancelled");
    }
    verifyOrderPayment(){
        console.log("Cannot verify. Its in Pending state");
    }
    shipProgress(){
        console.log("Cannot Ship. Its in Pending state");
    }
}

class PaymentReceieved implements State{
    state: String;
    order: State;
    constructor(state: State){
        this.state='Payment Receieved';
        this.order = state;
    }

    cancelOrder(){
        //this.order.id delete that record
        console.log("order Cancelled, returning payment");
    }

    verifyOrderPayment(){
        console.log("Payment recieved.");
    }

    shipProgress(){
        console.log("Going to be shipped");
    }
}

class OrderShipped implements State{
    state: String;
    order: State;
    constructor(state: State){
        this.state='Order Shipped';
        this.order = state;
    }

    cancelOrder(){
        //this.order.id delete that record
        console.log("order Shipped, cannot cancel");
    }

    verifyOrderPayment(){
        console.log("Payment recieved.");
    }

    shipProgress(){
        console.log("Going to be shipped");
    }
}

let order: State = new Order();

order = new PaymentReceieved(order);
order.shipProgress();
order.cancelOrder();
order.verifyOrderPayment();

order = new OrderShipped(order);
order.shipProgress();
order.cancelOrder();
order.verifyOrderPayment();




