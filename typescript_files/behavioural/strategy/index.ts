interface Strategy {
    execute(a, b);
}

class AddStrategy implements Strategy {
    execute(a: any, b: any) {
        return a + b;
    }

}

class SubStrategy implements Strategy {
    execute(a: any, b: any) {
        return a - b;
    }
}

class calculator implements Strategy {
    a: any;
    b: any;
    strategy: Strategy;
    constructor(a, b) {
        this.a = a;
        this.b = b;
    }
    execute(a: any, b: any) {
        return this.strategy.execute(this.a,this.b)
    }
    public setStrategy(strategy: Strategy) {
        this.strategy = strategy;
    }
}