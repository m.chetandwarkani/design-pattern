"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateAddonInDB = exports.updateMasterPlanInDB = exports.SubscriptionOptions = void 0;
const subs_1 = require("../mainclass/subs");
class SubscriptionOptions extends subs_1.Subscription {
}
exports.SubscriptionOptions = SubscriptionOptions;
class updateMasterPlanInDB extends SubscriptionOptions {
    constructor(decoratedSubs) {
        super();
        this.description = 'Master Plan updated';
        this.decoratedSubs = decoratedSubs;
        //update master plan name and details
    }
    price() {
        // fetch price for the relevant master plan
        // let fetched master plan price be = 3000
        return this.decoratedSubs.price() + 3000;
    }
}
exports.updateMasterPlanInDB = updateMasterPlanInDB;
class updateAddonInDB extends SubscriptionOptions {
    constructor(decoratedSubs) {
        super();
        this.description = 'Master Plan updated';
        this.decoratedSubs = decoratedSubs;
        //update addon plan name and details
    }
    price() {
        // fetch price for the relevant addon plan
        // let fetched master plan price be = 3000
        return this.decoratedSubs.price() + 1000;
    }
}
exports.updateAddonInDB = updateAddonInDB;
