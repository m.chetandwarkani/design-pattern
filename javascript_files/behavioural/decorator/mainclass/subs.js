"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Chargebee = exports.Stripe = exports.Subscription = void 0;
class Subscription {
    getdescription() {
        return this.description;
    }
    updateStripeSubscription() {
    }
    updateSubscription() {
    }
}
exports.Subscription = Subscription;
class Stripe extends Subscription {
    constructor() {
        super(...arguments);
        this.description = 'Stripe Subscription';
    }
    price() {
        return 0;
    }
    updateSubscription() {
        // update Stripe Subscription
    }
}
exports.Stripe = Stripe;
class Chargebee extends Subscription {
    constructor() {
        super(...arguments);
        this.description = 'Chargebee Subscription';
    }
    price() {
        return 0;
    }
    updateSubscription() {
        //// update chargebee Subscription
    }
}
exports.Chargebee = Chargebee;
