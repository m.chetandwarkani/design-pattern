"use strict";
class Stripewebhooks {
    constructor() {
        this.observers = [];
    }
    addObserver(observerSubject) {
        this.observers.push(observerSubject);
        return 'Observer added';
    }
    removeObserver(observe) {
        this.observers.pop();
        return 'Observer removed';
    }
    notify(observe) {
        for (let i = 0; i < this.observers.length; i++) {
            this.observers[i].update();
        }
        return 'Observer notified';
    }
}
class updateDB {
    update() {
        console.log('DB updated');
        return 'DB updated';
    }
}
class updateHubspot {
    update() {
        console.log('Hubspot updated');
        return 'DB updated';
    }
}
class mainclass {
    constructor() {
        let stripewebhooks = new Stripewebhooks();
        // let updatedb: ObserveSubject = new updateDB()
        let updatehubspot = new updateHubspot();
        // stripewebhooks.addObserver(updatedb)
        // stripewebhooks.addObserver(updatehubspot)
    }
}
