"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Resp = void 0;
class Resp {
    constructor(code, message) {
        this.code = this.validateCode(code);
        this.message = message;
    }
    validateCode(code) {
        if ([200, 300, 400, 401, 403, 500].includes(code)) {
            return code;
        }
        else {
            throw new Error("Code not found.");
        }
    }
}
exports.Resp = Resp;
