"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ormFactory_1 = require("./DAOImpl/ormFactory"); // use path npm
class tenant {
    constructor() {
        this.ormtenant = new ormFactory_1.OrmCreator().create('PRISMA');
        this.ormtenant2 = new ormFactory_1.OrmCreator().create('SEQUILIZE');
        console.log(" ---------------------------- Factory design pattern -----------------------");
        let resp = this.ormtenant.createTable();
        let resp2 = this.ormtenant2.createTable();
        console.log(" ---------------------------- End of Factory design pattern -----------------------");
    }
}
