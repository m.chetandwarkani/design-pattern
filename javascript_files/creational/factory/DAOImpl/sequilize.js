"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sequilize = void 0;
const Resp_1 = require("../Resp"); // use path npm
class Sequilize {
    constructor() {
        this.name = 'SEQUILIZE';
    }
    createTable() {
        //create table using Prisma
        return new Resp_1.Resp(200, 'created table using sequilize');
    }
    isIDPresent(tablename, id) {
        return new Resp_1.Resp(200, 'ID found in table using sequilize');
    }
}
exports.Sequilize = Sequilize;
