"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrmCreator = void 0;
const prisma_1 = require("./prisma"); // use path npm
const sequilize_1 = require("./sequilize"); // use path npm
class OrmCreator {
    create(objecttype) {
        switch (objecttype) {
            case 'PRISMA':
                return new prisma_1.Prisma();
            case 'SEQUILIZE':
                return new sequilize_1.Sequilize();
            default:
                return new sequilize_1.Sequilize();
        }
    }
}
exports.OrmCreator = OrmCreator;
