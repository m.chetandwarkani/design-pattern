"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Prisma = void 0;
const Resp_1 = require("../Resp"); // use path npm
class Prisma {
    constructor() {
        this.name = 'PRISMA';
    }
    createTable() {
        //create table using Prisma
        return new Resp_1.Resp(200, 'created table using Prisma');
    }
    isIDPresent(tablename, id) {
        return new Resp_1.Resp(200, 'ID found in table using Prisma');
    }
}
exports.Prisma = Prisma;
